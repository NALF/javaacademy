package com.softtek.academia.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.softtek.academia.model.ColorDTO;

public class ColorJDBCImpl implements ColorDAO{
	private static Connection CONNECTION;
	
    public ColorJDBCImpl() {
        // TODO Auto-generated constructor stub
    }
    private static void getConnection() {
    	if(CONNECTION == null) {
	    	 try  {
	    		 CONNECTION = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#",
	                 "root", "1234");
	    	 }catch (Exception e) {
	             e.printStackTrace();
	         }
    	 }
    }
    
    private PreparedStatement getReadyForQuery(String query) {
    	PreparedStatement preparedStatement=null;
    	ColorJDBCImpl.getConnection();
         // auto close connection
    	try {
             if (CONNECTION != null) {
                 System.out.println("Connected to the database!");
                 preparedStatement = CONNECTION.prepareStatement(query);
                 
             } else {
                 System.out.println("Failed to make connection!");
             }
    	}catch(Exception e) {
    		
    	}
        
         return preparedStatement;
    }
    
    @Override
    public List<ColorDTO> getAll() {
        System.out.println("MySQL JDBC Connection");
        List<ColorDTO> result = new ArrayList<>();
        String query = "SELECT * from COLOR";
            try {
                PreparedStatement preparedStatement = this.getReadyForQuery(query);
                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    ColorDTO color = new ColorDTO(resultSet.getLong("COLORID"), resultSet.getString("NAME"),
                            resultSet.getString("HEXVALUE"));
                    result.add(color);
                }
            } catch(Exception e) {
                System.out.println("Something went wrong");
            }
            return result;
        }
        
    
	@Override
	public ColorDTO getById(long id) {
		 ColorDTO result = null;
	        String query = "SELECT * from COLOR where colorId=?";
	            try {
	                PreparedStatement preparedStatement = this.getReadyForQuery(query);
	                preparedStatement.setLong(1, id);
	                ResultSet resultSet = preparedStatement.executeQuery();
	                while (resultSet.next()) {
	                    result = new ColorDTO(resultSet.getLong("COLORID"), resultSet.getString("NAME"),
	                            resultSet.getString("HEXVALUE"));
	                    
	                }
	            } catch(Exception e) {
	                System.out.println("Something went wrong");
	            }
	            return result;
	}
	
}
