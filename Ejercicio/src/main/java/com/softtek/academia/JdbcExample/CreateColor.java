package com.softtek.academia.JdbcExample;

@FunctionalInterface
public interface CreateColor <R>{
	public R apply(Long id,String a, String b);
}
