package com.softtek.academia.JdbcExample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.softtek.academia.dao.ColorJDBCImpl;
import com.softtek.academia.model.ColorDTO;
import com.softtek.academia.service.ColorServiceImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	/*System.out.println("MySQL JDBC Connection");

        List<Object> result = new ArrayList<>();
        List<Color> colores = new ArrayList<>();

        String SQL_SELECT = "Select * from COLOR";
        
    	// auto close connection
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {

            if (conn != null) {
                System.out.println("Connected to the database!");
                
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

                ResultSet resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) { //iterable
                	
                    long id = resultSet.getLong("COLORID");
                    String name = resultSet.getString("NAME");
                    String hexValue = resultSet.getString("HEXVALUE");
                   
                    colores.add(new Color(id,name,hexValue));
                    //System.out.println("ID: " + id + " color's name: " + name + " hex's value: " + hexValue);
                }
                //PRIMERA FORMA
                for(Color c : colores) {
                	System.out.println(c.toString());
                }
                //SEGUNDA FORMA
                colores.forEach(System.out::println);
                System.out.println("--------------");
                
                
               
                //Modificar un método que busca un color basado en su ID y transformarlo en una expresión lambda
                SearchFuncti<Integer, Color> search= (id) -> {
                	for(int i=0;i<colores.size();i++) {
                		if(colores.get(i).getId()==id) return colores.get(i) ;
                	}
                	return null;
                };
                //
        		BiFunction<List<Color>, Long, Color> searchFunc = (list, l)->{
        			for(Color c : list) {
        				if(l == c.getId()) return c;
        			}
        			return null;
        		};
        		searchFunc.apply(colores, 1l);
        		
        		
                //Crear predicado que vea si un hex color está en la lista
                Predicate<String> pred = (s) -> {
                	for(int i=0;i<colores.size();i++) {
                		if(colores.get(i).getHexValue().equals(s)) return true;
                	}
					return false;
                };
                //
                BiPredicate<List<Color>, String> pred2 = (l,s) -> {
                	for(Color co : l) {
                		if(co.getHexValue().equals(s)) return true;
                	}
					return false;
                };
                
                // Crear supplier que genere un Color object pasando el nombre y el hex value
                CreateColor<Color> create= (id,nom,hex) -> new Color(id,nom,hex);
                
                
        		//Crear un consumer que imprima todos los elementos de la lista y que solo imprima el valor que se especifica como parámetro
        		Consumer <Integer> consumer = (value) -> {
        			for(int i=0;i<colores.size();i++) {
                		if(value.equals("id")) {
                			System.out.println(colores.get(i).getId());
                		}
                		if(value.equals("name")) {
                			System.out.println(colores.get(i).getName());
                		}
                	}
        		};
        		//
        		BiConsumer <List<Color>,String> consumer2 = (l,v) -> {
        			for(Color col : l) {
        				switch(v) {
        					case "id":  
        						System.out.println(col.getId());
        						break;
        					case "name": 
        						System.out.println(col.getName());
        						break;
        					case "hex":
        						System.out.println(col.getHexValue());
        						break;
        					default: 
        						System.out.println(col.getHexValue());
        						break;
        					
        				}
        			}
        				
        		};
                
        		//STREAMS
        		//Map the color object into a string using the hexvalue
        		System.out.println("STREAMS");
        		colores.stream()
        			//.map((color) -> color.getHexValue()) //Se utiliza cuando se convierten datos de un tipo a otro
        			//.map(Color::getHexValue)	
        			.map(Color::getName)	
        			.forEach(System.out::println);
        		
        		Long sumatoria = colores.stream()
        			.map(Color::getId)
        			.reduce(0L,(id1, id2) -> id1 + id2);
        		System.out.println("La suma de los ids es: "+sumatoria);
        		
        		
        		Optional<Long> sumatoria2 = colores.stream()
            			.map(Color::getId)
            			.reduce((id1, id2) -> id1 + id2);
            		System.out.println("La suma de los ids es: "+sumatoria2.get());
        		if(sumatoria2.isPresent()) {
        			
        		}
        		
        		//Filter those colors that begin with a letter
        		List<String> lista= colores.stream()
        			.map((color) -> color.getHexValue())
        			.filter(color -> Character.isAlphabetic(color.charAt(1))) 
        			.collect(Collectors.toList());
        		//
        		List <String> lista3 = colores.stream()
        				.map(Color::getHexValue)
        				.filter((h)->{
        					Pattern p = Pattern.compile(".[A-F].*");
        					Matcher m = p.matcher(h.substring(1));
        					return m.find();
        				})
        				.collect(Collectors.toList());
        		
        		
        		//Create a map object using the hexValue as key and the name as value
        		Map<String, String> lista2= colores.stream()
        				.collect(
        						Collectors.toMap(Color::getHexValue,Color::getName,
        								(s,a)->s+", "+a)
        				);
        		System.out.println(lista2.values().toString());

        		
        		
        		
            } else {
                System.out.println("Failed to make connection!");
            }

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }  */
    	System.out.println("MySQL JDBC Connection");
    	ColorJDBCImpl colorDao = new ColorJDBCImpl();
    	ColorServiceImpl colorService = new ColorServiceImpl(colorDao);
    	List <ColorDTO> colors = colorService.getAll();
    	System.out.println(colorDao.getById(2));
    	colors.forEach(System.out::println);
    }
}
