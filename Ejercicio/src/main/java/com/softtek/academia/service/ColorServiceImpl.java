package com.softtek.academia.service;

import java.util.List;
import java.util.stream.Collectors;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.model.ColorDTO;

public class ColorServiceImpl implements ColorService{
	
	private ColorDAO colorDao;
	
	public ColorServiceImpl (ColorDAO color) {
		this.colorDao=color;
	}
	
	@Override
	public List<ColorDTO> getAll() {
		return colorDao.getAll();
	}

	@Override
	public ColorDTO getById(long id) {
		return colorDao.getById(id);
	}

	@Override
	public List<ColorDTO> getFilterList() {
		List<ColorDTO> colors = colorDao.getAll();
		colors.removeIf((color)->color.getName().equals("Black"));
		/*colors = colors.stream()
			.filter((c)-> !c.getName().equals("Black"))
			.collect(Collectors.toList());*/
		return colors;
	}
	
}
