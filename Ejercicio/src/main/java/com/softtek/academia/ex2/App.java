package com.softtek.academia.ex2;

import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class App {

	public static void main(String[] args) {
		/*Connection con;
		con= DBConnection2.connect();
		ArrayList<Device> devices =DBConnection2.getDevices(con);*/
		
		DBConnection conn = new DBConnection();
		ArrayList<Device> devices = Device.getDevices(conn.connect());
		
		List<String> laptops =devices.stream()
			.map(Device::getName)
			.filter(c -> c.toUpperCase().contains("LAPTOP"))
			.collect(Collectors.toList());
		
		List<Device> devs = devices.stream()
			.filter(c -> c.getColor()==1)
			.peek(System.out::println)
			.collect(Collectors.toList());
		
		Long num =devices.stream()
			.filter(d ->d.getManuf()==3)
			.count();
		System.out.println(num);
		
		Map<Integer, Device> mapa = devices.stream()
				.collect(
						Collectors.toMap(Device::getId, Device::getDev)
				);
		System.out.println(mapa.values().toString());
		
		//EXCEL
				WriteExcel excelWriter = new WriteExcel();
		   	 
		    	
		    	String excelFilePath = "Books.xls";
		    	 
		    	try {
					excelWriter.writeExcel(devices, excelFilePath);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
	}

}
