package com.softtek.academia.ex2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Device {
	int id;
	String name;
	String description;
	int manufacturer;
	int color;
	
	public Device(int i, String n, String d, int m, int c) {
		this.id=i;
		this.name=n;
		this.description=d;
		this.manufacturer=m;
		this.color=c;
	}
	public Device getDev() {
		return this;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getDesc() {
		return description;
	}
	public int getManuf() {
		return manufacturer;
	}
	public int getColor() {
		return color;
	}
	public void setId(int id) {
		this.id=id;
	}
	public void setName(String name) {
		this.name=name;
	}
	public void setDesc(String desc) {
		this.description=desc;
	}
	public void setManu(int manu) {
		this.manufacturer=manu;
	}
	public void setColor(int col) {
		this.color=col;
	}
	@Override
	public String toString() {
		return "Device [id=" + id + ", name=" + name + ", description=" + description + ", manufacturer=" + manufacturer
				+ ", color=" + color + "]";
	}
	public static ArrayList<Device> getDevices(Connection conn){
		ArrayList<Device> list = new ArrayList<>();
		String SQL_SELECT = "Select * from DEVICE";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = conn.prepareStatement(SQL_SELECT);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) { 
	        	
	            int id = resultSet.getInt("DEVICEID");
	            String name = resultSet.getString("NAME");
	            String desc = resultSet.getString("DESCRIPTION");
	            int manu = resultSet.getInt("MANUFACTURERID");
	            int color = resultSet.getInt("COLORID");
	           
	            list.add(new Device(id,name,desc,manu,color));
	            
	        }
	        
	        return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	
		
	
		
        
	}
}
