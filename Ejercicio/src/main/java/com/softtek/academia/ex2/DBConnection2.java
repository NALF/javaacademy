package com.softtek.academia.ex2;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.softtek.academia.JdbcExample.Color;
import com.softtek.academia.JdbcExample.CreateColor;
import com.softtek.academia.JdbcExample.SearchFuncti;
/*******************ESTA NO SE USA************************/
public interface DBConnection2 {
	
	static Connection connect() {
		try {
			Connection conn = DriverManager.getConnection(
	                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
			System.out.println("Connected to the database!");
			return conn;
		}catch(Exception e) {
			return null;
		}
	}
	
	static ArrayList<Device> getDevices(Connection conn){
		ArrayList<Device> list = new ArrayList<>();
		String SQL_SELECT = "Select * from DEVICE";
		PreparedStatement preparedStatement;
		try {
			preparedStatement = conn.prepareStatement(SQL_SELECT);
			ResultSet resultSet = preparedStatement.executeQuery();
			
			while (resultSet.next()) { 
	        	
	            int id = resultSet.getInt("DEVICEID");
	            String name = resultSet.getString("NAME");
	            String desc = resultSet.getString("DESCRIPTION");
	            int manu = resultSet.getInt("MANUFACTURERID");
	            int color = resultSet.getInt("COLORID");
	           
	            list.add(new Device(id,name,desc,manu,color));
	            
	        }
	        
	        return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	
		
	
		
        
	}
	
}
