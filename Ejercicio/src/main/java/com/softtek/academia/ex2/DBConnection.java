package com.softtek.academia.ex2;


import java.sql.Connection;
import java.sql.DriverManager;


public class DBConnection{
	private static Connection conn = null;
	
	public DBConnection() {}
	
	public static Connection connect() {
		if(conn==null) {
			try {
				 conn = DriverManager.getConnection(
		                "jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234");
				System.out.println("Connected to the database!");
						}catch(Exception e) {
				System.out.println("Error with connection");
			}
		}
		return conn;
	}
	
	
	
}
