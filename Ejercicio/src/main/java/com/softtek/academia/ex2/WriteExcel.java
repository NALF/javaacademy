package com.softtek.academia.ex2;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel {
	
	public void writeExcel(List<Device> listDev, String excelFilePath) throws IOException {
	    Workbook workbook = new HSSFWorkbook();
	    Sheet sheet = workbook.createSheet();
	 
	    int rowCount = 0;
	 
	    for (Device aBook : listDev) {
	        Row row = sheet.createRow(++rowCount);
	        writeBook(aBook, row);
	    }
	 
	    try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
	        workbook.write(outputStream);
	    }
	}
	private void writeBook(Device dev, Row row) {
	    Cell cell = row.createCell(1);
	    cell.setCellValue(dev.getId());
	 
	    cell = row.createCell(2);
	    cell.setCellValue(dev.getName());
	    cell = row.createCell(3);
	    cell.setCellValue(dev.getDesc());
	    cell = row.createCell(4);
	    cell.setCellValue(dev.getManuf());
	    cell = row.createCell(5);
	    cell.setCellValue(dev.getColor());
	}
	
}
