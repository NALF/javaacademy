package com.softtek.academia.model;

public class ColorDTO {
	 private long id;
     private String name;
     private String hexValue;
     
     public ColorDTO() {
    	 
     }
     
     public ColorDTO(long id, String name, String hexValue) {
		this.id = id;
		this.name = name;
		this.hexValue = hexValue;
	}
     
	public long getId() {
 		return id;
 	}
 	public void setId(long id) {
 		this.id = id;
 	}
 	public String getName() {
 		return name;
 	}
 	public void setName(String name) {
 		this.name = name;
 	}
 	
 	public String getHexValue() {
 		return hexValue;
 	}
 	public void setHexValue(String hexValue) {
 		this.hexValue = hexValue;
 	}

 	@Override
 	public String toString() {
 		return "Color [id=" + id + ", name=" + name + ", hexValue=" + hexValue + "]";
 	}
}
