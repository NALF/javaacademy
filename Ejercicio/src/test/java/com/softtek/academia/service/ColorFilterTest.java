package com.softtek.academia.service;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.List;

import org.junit.jupiter.api.Test;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.dao.ColorJDBCImpl;
import com.softtek.academia.model.ColorDTO;

public class ColorFilterTest {
	
	@Test
	public void testSinNegro() {
		//setup
		ColorDAO colorDao = new ColorJDBCImpl();
		ColorService colorService= new ColorServiceImpl(colorDao);
		List<ColorDTO> filteredList = colorService.getFilterList();
		
		assertNotNull(filteredList,"Filtered list is empty");
		filteredList.forEach((c)->{
			assertNotEquals("Black", c.getName());
		});
	}

}
