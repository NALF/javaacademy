package com.softtek.academia.JdbcExample;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import com.softtek.academia.ex2.DBConnection;

/**
 * Unit test for simple App.
 */
public class AppTest {
	@Rule
	  public final ExpectedException exception = ExpectedException.none();
	
	@Test 
	public void testConnection() {
		assertNotNull(DBConnection.connect());
		exception.expect(SQLException.class);
		//assertThrows(SQLException.class, () 
				//-> DBConnection.connect(),"Error de conexion");
	}
		
		
}
