package com.softtek.academia.JdbcExample;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.util.List;


import org.junit.jupiter.api.*;

import com.softtek.academia.dao.ColorDAO;
import com.softtek.academia.dao.ColorJDBCImpl;
import com.softtek.academia.model.ColorDTO;
public class ColorDaoTest {
	
	private ColorDAO colorDao;
	
	@BeforeEach
	public void initJDBC() {
		this.colorDao = new ColorJDBCImpl();
	}
	@Test
	public void testGetAllColors() {
		//setup
		ColorDTO color = new ColorDTO(1,"White","#FFFFFF");
		//execute
		List<ColorDTO> colors = colorDao.getAll();
		
		//verify
		assertNotNull(colors,"Color list is empty");
		ColorDTO actualColor =colors.get(0);
		assertEquals(color.getName(),actualColor.getName());
	}
	
	@Test
	public void testGetColorById() {
		//setup
		long id = 2;
		ColorDTO expectedColor = new ColorDTO(2,"Silver","#C0C0C0");
		
		//EXECUTE
		ColorDTO actualColor = colorDao.getById(id);
		
		//VALIDATE
		assertNotNull(actualColor, "Color is empty");
		assertEquals(expectedColor.getName(),actualColor.getName());
	}
}
